const jwt = require("jsonwebtoken");
const express = require('express');
const router = express.Router();
const models = require('../models');
const app = require('../app');
const config = require('../config/config');

router.post('/', (req, res) => {
    const {name, password} = req.body;
    models.User
        .findOne({where: {name: req.body.name}})
        .then(user => {
            user.checkPassword(req.body.password, user.password, (err, isMatch) =>{
                if(err){
                    console.log(err);
                    res.sendStatus(500).json({
                        title: 'Login failed',
                        error: {message: 'Invalid login credentials'}
                    });
                }
                else{
                    res.status(200).json({ 
                        userId: user.id,
                        token: jwt.sign({ name: user.name, age: user.age, occupation: user.oc}, config.secret, {
                            expiresIn: "24h" 
                        })
                    }); 
                }
            })
        })
        .catch(err => console.log(err));
});

router.post('/register', (req,res) =>{
    
    models.User
        .create({ name: req.body.name, password: req.body.password, 
            age: req.body.age, occupation: req.body.occupation})
        .then(res.sendStatus(201))
        .catch(err => {console.log(err),
        res.status(500).json({
            title: 'Singin Up failed',
            error: {message: 'Wrong user name'}
        })})
})

module.exports = router;
