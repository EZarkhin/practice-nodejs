const models = require('../models');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const config = require('../config/config');

router.get('/pos/:userId', (req, res) => {
    models.Location
        .findAll({attributes: ['lat','lng'],
            where: {
            UserId: req.params.userId
          }})
        .then(location => {
            res.json(location)})
        .catch(err => res.status(404).send('Location not found.'));
});

router.get('/info/:userId', (req, res) => {
    models.Location
        .findAll({attributes: ['name','type'],
            where: {
            UserId: req.params.userId
          }})
        .then(location => {
            res.json(location)})
        .catch(err => res.status(404).send('Location not found.'));
});

router.post('/create', (req,res)=>{
   
   jwt.verify(req.query.token, config.secret, (err, decoded)=>{
       if (err){
            res.status(500).json({
                title: 'Authentication failed'
            })
       }
       else {
        models.Location
        .create({name: req.body.name, type: req.body.type, lat: req.body.lat, lng: req.body.lng, UserId: req.body.userId
        })
        .then(res.sendStatus(201))
        .catch(err => {console.log(err),
            res.status(500).json({
                title: 'Creating location failed',
                error: {message: 'Wrong data'}
            })})
       }
   })
    
})


module.exports = router;