const models = require('../models');
const express = require('express');
const router = express.Router();
const config = require('../config/config')
const jwt = require('jsonwebtoken')

router.get('/:id', (req, res) => {
    models.User
        .findOne({where: {
            id: req.params.id
          }})
        .then(user => {
            res.json(user)})
        .catch(err => res.status(404).send('User not found.'));
});

router.put('/:id', (req,res) =>{
    jwt.verify(req.query.token, config.secret, (err, decoded)=>{
        if (err){
             res.status(500).json({
                 title: 'Authentication failed'
             })
        } else {
            models.User
                    .findOne({where:{
                        id: req.params.id
                    }})
                    .then(user=> user.update({
                        name: req.body.name,
                        age: req.body.age,
                        occupation: req.body.occupation
                    }))
                    .catch(err => res.sendStatus(404));
        }
})})

router.delete('/:id', (req,res)=>{
    jwt.verify(req.query.token, config.secret, (err, decoded)=>{
        if (err){
             res.status(500).json({
                 title: 'Authentication failed'
             })
        } else {
            models.User
                    .findOne({where:{
                        id: req.params.id
                    }})
                    .then(user=> user.destroy())
                    .catch(err => res.sendStatus(404));
        }
})})

module.exports = router;
